/**
 * Gets the name of the current media query as set in CSS e.g. "small" | "medium" | "desktop".
 * Works with IE6-8 (with respond.js), IE9+, Firefox 4+, Chrome 14+, Opera 11+, Possibly older versions Chrome and Opera too.
 *
 * @author Stephen S
 * @return {String} media - Query name or empty string if none found
 */
(this.define || Object)(
    (this.exports || this).mediaAlias =

    (function() {
        var head = document.getElementsByTagName('head')[0],
            styles = head.currentStyle || window.getComputedStyle(head),
            rQuotes = /["'\. ]/g;

        return function() {
            //Check quotes property first as it always returs the correct value if supported. If we checked fontFamily
            //first we could end up with "Times new roman" in some versions of Opera.
            return (styles.quotes || styles.fontFamily).replace(rQuotes, '');
        };
    }())
);
