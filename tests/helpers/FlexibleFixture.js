// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.FlexibleFixture = factory();
  }
}(this, function () {
    'use strict';

    function extend(receiver, supplier) {
        var property;

        for (property in supplier) {
            if (supplier.hasOwnProperty(property)) {
                receiver[property] = supplier[property];
            }
        }

        return receiver;
    }
    
    function FlexibleFixture(options) {
        
        this.options = options = extend({
            document: document,
            window: window,
            width: 0,
            height: 0
        }, options);
      
        this.iframe = document.createElement('iframe');
        this.iframe.scrolling = 'no';
        this.iframe.style.overflow = 'hidden';
        this.iframe.width = this.options.width;
        this.iframe.height = this.options.height;
        
        if ('onload' in this.options) {
            this.onload(this.options.onload);
        }
        
        if ('src' in this.options) {
            this.iframe.src = this.options.src;
        }
        
        document.body.appendChild(this.iframe); 
        
        this.window = this.iframe.contentWindow || this.iframe.contentDocument.parentWindow;
        this.window.flexibleFixture = this;
                
        //log errors using parent's onerror
        this.window.onerror = onerror;

        //log with the parent's console
        this.window.console = console;       
    }
    
    FlexibleFixture.prototype.onload = function(handler) {
        
        var self = this,
            _handler = function(ev) {
                handler.call(this, ev, self);
            };
        
        //xbrowser iframe onload:
        //http://www.nczonline.net/blog/2009/09/15/iframes-onload-and-documentdomain/
        if (this.iframe.attachEvent) {
            this.iframe.attachEvent('onload', _handler);
        } else {
            this.iframe.onload = _handler;
        }
    };
    
    FlexibleFixture.prototype.resize = function(options) {
        
        var self = this;

        function resized() {
            
            if (options.widths.length) {
                self.iframe.width = options.widths.shift();
                
                if ('resize' in options) {
                    options.resize(parseInt(self.iframe.width, 10));
                }
            } else {
                delete self.resized;
                options.complete();
            }
        };
        
        this.resized = resized;

        //ie8 and phantomjs require the setTimeout
        //http://stackoverflow.com/questions/779379/why-is-settimeoutfn-0-sometimes-useful
        setTimeout(resized, 0);
    };
    
    return FlexibleFixture;
}));
