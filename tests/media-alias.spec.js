describe('mediaAlias', function() {

    /*xit('should get the currently active media query alias as set in CSS', function(){

        //These media queries are in the fixtures css file included by karma.
        //We can't reliably know what size the window will be opened at, so
        //instead we check that at least one of the four media queries match.
        expect(mediaAlias()).toMatch(/^(?:xx-small|small|medium|large)$/);
    });*/
    
    it('should get the currently active media query alias as set in CSS 2', function(done){
        
        
        new FlexibleFixture({
            src: 'http://localhost:9876/base/tests/fixtures/resize.html', 
            onload: function(ev, flexibleFixture) {
                flexibleFixture.resize({
                    widths: [100],
                    resize: function() {},
                    complete: function() {
                        
                        expect(flexibleFixture.window.mediaAlias()).toEqual('xx-small');
                        done();  
                    }
                });
            } 
        });
        
    });
    
    it('should get the currently active media query alias as set in CSS 3', function(done){
        
        
        new FlexibleFixture({
            src: 'http://localhost:9876/base/tests/fixtures/resize.html', 
            onload: function(ev, flexibleFixture) {
                flexibleFixture.resize({
                    widths: [220],
                    resize: function() {},
                    complete: function() {
                        
                        expect(flexibleFixture.window.mediaAlias()).toEqual('small');
                        done();  
                    }
                });
            } 
        });
    });
    
    /*it('should get the currently active media query alias as set in CSS 3', function(done){
        
        
        var c = new FlexibleFixture({
            src: 'http://localhost:9876/base/tests/fixtures/resize.html', 
            onload: function(ev, flexibleFixture) {
                c.resize({
                    widths: [200],
                    resize: function() {},
                    complete: function() {
                        expect(c.window.mediaAlias()).toEqual('small');
                        done();  
                    }
                });
            } 
        });
        
        
        console.log('c: ', c.sjs);
    });*/
});

